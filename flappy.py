"""Flappy, game inspired by Flappy Bird.

Exercises

1. Keep score.
2. Vary the speed.
3. Vary the size of the balls.
4. Allow the bird to move forward and back.

"""

from random import *
from turtle import *

from freegames import vector

#
DRAW_TO_SCREEN = Turtle(visible=False)
GAME_STATE = {'score': 0}
bird = vector(0, 0)
balls = []


# ball object:
class BallObj:
    def __init__(self, ball_speed, ball_size, ball_y_coordinate):
        self.speed = ball_speed
        self.size = ball_size
        self.vector = vector(199, ball_y_coordinate)


def bird_move(x, y):
    """
    :param x: how many pixels to move in the x direction
    :param y: how many pixels to move in the y direction
    """
    GAME_STATE['score'] += 1  # increase the score in each key press(up, left or right).
    move_vector = vector(x, y)
    bird.move(move_vector)


def inside(point):
    "Return True if point on screen."
    return -200 < point.x < 200 and -200 < point.y < 200


def draw(alive):
    "Draw screen objects."
    clear()

    goto(bird.x, bird.y)

    # in case game is still on.
    if alive:
        DRAW_TO_SCREEN.undo()  # delete the score from screen.
        DRAW_TO_SCREEN.color("green")
        DRAW_TO_SCREEN.write("Score: {}".format(GAME_STATE["score"]))  # prints to the screen the updated score.
        dot(10, 'green')

    # in case game is over
    else:
        DRAW_TO_SCREEN.undo()  # delete the score from screen.
        DRAW_TO_SCREEN.color("red")  # change the text color to red.
        # prints to the screen the final score.
        DRAW_TO_SCREEN.write("Game finished with Score: {}".format(GAME_STATE["score"]))
        dot(10, 'red')
        for ball in balls:
            ball.size = 0

    for ball in balls:
        goto(ball.vector.x, ball.vector.y)
        dot(ball.size, "black")

    update()


def move():
    "Update object positions."
    bird.y -= 5
    for ball in balls:
        ball.vector.x -= ball.speed

    if randrange(10) == 0:
        y = randrange(-199, 199)  # randomize ball initialized y coordinate.
        size = randint(10, 50)  # randomize ball size.
        speed = randint(3, 10)  # randomize ball speed.
        ball = BallObj(speed, size, y)  # initialize ball object.
        balls.append(ball)  # add ball to balls array

    while len(balls) > 0 and not inside(balls[0].vector):
        balls.pop(0)

    if not inside(bird):
        draw(False)
        return

    for ball in balls:
        if abs(ball.vector - bird) < 15:
            draw(False)
            return

    draw(True)
    ontimer(move, 50)


setup(420, 420, 900, 300)
hideturtle()
up()
tracer(False)
# listen to key press - up, left and right keys.
listen()
onkeypress(lambda: bird_move(10, 0), "Right")
onkeypress(lambda: bird_move(-10, 0), "Left")
onkeypress(lambda: bird_move(0, 30), "Up")
move()
done()
